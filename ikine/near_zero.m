function result = near_zero(value)
result = norm(value) < 1e-6;
end
