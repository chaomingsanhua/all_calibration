function vec = so3_to_vec(so3)
vec = [so3(3, 2); so3(1, 3); so3(2, 1)];
end