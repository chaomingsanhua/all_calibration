function error_x = calculate_error(delta_para, DH_para, x_real, Q)
[n, ~] = size(Q);

Alpha = DH_para.Alpha + delta_para(1:6);
A = DH_para.A + delta_para(7:12);
D = DH_para.D + delta_para(13:18);
Theta = DH_para.Theta + delta_para(19:24);

for i = 1:6
    L(i) = Link('alpha',Alpha(i),'a', A(i),'d', D(i)); L(i).offset = Theta(i); L(i).qlim=[-pi, pi]; L(i).mdh = 1;
end

robot = SerialLink(L, 'name', 'UR');
for i = 1:n
    qi = Q(i, :);
    x_th(3*i-2:3*i, 1) = robot.fkine(qi).t;
end
error_x = x_real - x_th;