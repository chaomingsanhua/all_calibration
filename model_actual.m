%% actual model

d1 = 0.089159; d4 = 0.10915; d5 = 0.09465; d6 = 0.0823;
a3 = 0.425; a4 = 0.39225;

delta_alpha1 = 0.01; delta_alpha2 = 0.02; delta_alpha3 = 0.03; 
delta_alpha4 = -0.01; delta_alpha5 = -0.02; delta_alpha6 = 0; 

delta_a1 = 0.01; delta_a2 = 0.02; delta_a3 = 0.03;
delta_a4 = -0.01; delta_a5 = -0.02; delta_a6 = 0; 

delta_d1 = 0.01; delta_d2 = 0.02; delta_d3 = 0.03;
delta_d4 = -0.01; delta_d5 = -0.02; delta_d6 = -0.03;

delta_theta1 = 0.01; delta_theta2 = 0.02; delta_theta3 = 0.03; 
delta_theta4 = -0.01; delta_theta5 = -0.02; delta_theta6 = 0;

Alpha = [0 + delta_alpha1, -pi/2 + delta_alpha2, 0 + delta_alpha3, 0 + delta_alpha4, pi/2 + delta_alpha5, -pi/2 + delta_alpha6];
A = [0 + delta_a1, 0 + delta_a2, a3 + delta_a3, a4 + delta_a4, 0 + delta_a5, 0 + delta_a6];
D = [d1 + delta_d1, 0 + delta_d2, 0 + delta_d3, d4 + delta_d4, d5 + delta_d5, d6 + delta_d6];
Theta = [0 + delta_theta1, -pi/2 + delta_theta2, 0 + delta_theta3, pi/2 + delta_theta4, 0 + delta_theta5, 0 + delta_theta6];
Sigma = [1, 1, 1, 1, 1, 1];

for i = 1:6
    L(i) = Link('alpha',Alpha(i),'a', A(i),'d', D(i));L(i).offset = Theta(i); L(i).qlim=[-pi, pi];L(i).mdh = 1;
end

robot_real = SerialLink(L, 'name', 'UR');

DH_Para.Alpha_real = Alpha;
DH_Para.A_real = A;
DH_Para.D_real = D;
DH_Para.Theta_real = Theta;
DH_Para.Sigma_real = Sigma;